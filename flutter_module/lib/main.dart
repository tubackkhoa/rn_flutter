import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

const MethodChannel _channel = const MethodChannel("main_channel");

Widget goRN = RaisedButton(
  onPressed: () async {
    // Navigate back to the first screen by popping the current route
    // off the stack.
    // SystemNavigator.pop();
    if (Platform.isAndroid) {
      await SystemChannels.platform
          .invokeMethod<void>('SystemNavigator.pop', true);
    } else {
      await _channel.invokeMethod('startRNActivity');
    }
  },
  child: Text('Go To RN'),
);

void main() {
  runApp(MaterialApp(
    title: 'Named Routes Demo',
    // Start the app with the "/" named route. In this case, the app starts
    // on the FirstScreen widget.
    initialRoute: '/',
    routes: {
      // When navigating to the "/" route, build the FirstScreen widget.
      '/': (context) => FirstScreen(),
      // When navigating to the "/second" route, build the SecondScreen widget.
      '/second': (context) => SecondScreen(),
    },
  ));
}

class FirstScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('First Screen'),
      ),
      body: Column(children: [
        RaisedButton(
          child: Text('Launch Second'),
          onPressed: () {
            // Navigate to the second screen using a named route.
            Navigator.pushNamed(context, '/second');
          },
        ),
        goRN,
      ]),
    );
  }
}

class SecondScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Screen"),
      ),
      body: Column(children: [
        RaisedButton(
          onPressed: () {
            // Navigate back to the first screen by popping the current route
            // off the stack.
            Navigator.pop(context);
          },
          child: Text('Go back!'),
        ),
        goRN
      ]),
    );
  }
}
