//
//  ChangeViewBridge.m
//  WithFlutter
//
//  Created by Thanh Tu on 6/18/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChangeViewBridge.h"
#import "AppDelegate.h"

@implementation ChangeViewBridge

// reference "ChangeViewBridge" module in index.ios.js
RCT_EXPORT_MODULE(ChangeViewBridge);

- (dispatch_queue_t)methodQueue
{
  return dispatch_get_main_queue();
}

RCT_EXPORT_METHOD(changeToNativeView) {
  
  AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;  
  [appDelegate showFlutter];
}

@end
