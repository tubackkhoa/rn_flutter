#import "AppDelegate.h"
#import <ReactNativeNavigation/ReactNativeNavigation.h>
#import <React/RCTBundleURLProvider.h>
#import <FlutterPluginRegistrant/GeneratedPluginRegistrant.h>

#if RCT_DEV
#import <React/RCTDevLoadingView.h>
#endif

//
//#if DEBUG
//#import <FlipperKit/FlipperClient.h>
//#import <FlipperKitLayoutPlugin/FlipperKitLayoutPlugin.h>
//#import <FlipperKitUserDefaultsPlugin/FKUserDefaultsPlugin.h>
//#import <FlipperKitNetworkPlugin/FlipperKitNetworkPlugin.h>
//#import <SKIOSNetworkPlugin/SKIOSNetworkAdapter.h>
//#import <FlipperKitReactPlugin/FlipperKitReactPlugin.h>
//
//static void InitializeFlipper(UIApplication *application) {
//  FlipperClient *client = [FlipperClient sharedClient];
//  SKDescriptorMapper *layoutDescriptorMapper = [[SKDescriptorMapper alloc] initWithDefaults];
//  [client addPlugin:[[FlipperKitLayoutPlugin alloc] initWithRootNode:application withDescriptorMapper:layoutDescriptorMapper]];
//  [client addPlugin:[[FKUserDefaultsPlugin alloc] initWithSuiteName:nil]];
//  [client addPlugin:[FlipperKitReactPlugin new]];
//  [client addPlugin:[[FlipperKitNetworkPlugin alloc] initWithNetworkAdapter:[SKIOSNetworkAdapter new]]];
//  [client start];
//}
//#endif


@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//#if DEBUG
//  InitializeFlipper(application);
//#endif
  
  self.flutterEngine = [[FlutterEngine alloc] initWithName:@"flutter_module"];
  
  FlutterMethodChannel* methodChannel = [FlutterMethodChannel methodChannelWithName:@"main_channel"  binaryMessenger:self.flutterEngine.binaryMessenger];
  
//  [[self.flutterEngine navigationChannel] invokeMethod:@"setInitialRoute" arguments:@"/"];
  
  // Runs the default Dart entrypoint with a default Flutter route.
  [self.flutterEngine run];
  

  __weak typeof(self) weakSelf = self;
  [methodChannel setMethodCallHandler:^(FlutterMethodCall* call, FlutterResult result) {
      if ([@"startRNActivity" isEqualToString:call.method]) {
//        weakSelf.window.rootViewController = rnController;
        [weakSelf.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
        result(@(YES));
      } else {
        result(FlutterMethodNotImplemented);
      }

  }];
  
  // Used to connect plugins (only if you have plugins with iOS platform code).
  [GeneratedPluginRegistrant registerWithRegistry:self.flutterEngine];

  [ReactNativeNavigation bootstrapWithDelegate:self launchOptions:launchOptions];
  
  #if RCT_DEV
    [[ReactNativeNavigation getBridge] moduleForClass:[RCTDevLoadingView class]];
  #endif
  
  
  
  return YES;
}

- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge
{
  
#if DEBUG
  return [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
#else
  return [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
#endif
  
  
}


- (void)showFlutter {
    
  FlutterViewController *flutterViewController =
  [[FlutterViewController alloc] initWithEngine:self.flutterEngine nibName:nil bundle:nil];
  flutterViewController.view.backgroundColor = UIColor.whiteColor;
  flutterViewController.modalPresentationStyle = UIModalPresentationFullScreen;
  
  NSLog(@"RN binding - Native View - Loading FlutterViewController");
  
  [self.window.rootViewController presentViewController:flutterViewController animated:YES completion:nil];

}

@end
