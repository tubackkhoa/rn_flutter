//
//  ChangeViewBridge.h
//  WithFlutter
//
//  Created by Thanh Tu on 6/18/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

#ifndef ChangeViewBridge_h
#define ChangeViewBridge_h

#import <React/RCTBridgeModule.h>

@interface ChangeViewBridge : NSObject <RCTBridgeModule>

- (void) changeToNativeView;

@end

#endif /* ChangeViewBridge_h */
